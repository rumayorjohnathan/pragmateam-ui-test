export interface Beer {

    currentTemperature: number;
​​
    currentTemperatureOutOfRange: boolean
​​
    description: string
​​
    id: number
​​
    maxTemperature: number
​​
    minTemperature: number
​​
    variety: string

}