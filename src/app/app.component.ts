import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, interval, Subscription } from 'rxjs';
import { Beer } from './beer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit {

  private updateSubscription: Subscription;
  beers: Beer[];
  title = 'pragmateam-ui';
  constructor(private httpClient: HttpClient) {
  }
  
  ngOnInit(): void {
    this.updateSubscription = interval(10000).subscribe(
      (val) => { this.update()
    }
    );
  }


  update() {
    this.httpClient.get('http://localhost:8080/pragmabrewery-web/rest/beers')
    .subscribe(
      (res: Beer[]) => { 
        console.log(res);
        this.beers = res;
      })

  }

}
